//
//  SigninViewController.swift
//  User
//
//  Created by admin on 18/01/20.
//  Copyright © 2020 admin. All rights reserved.
//

import UIKit
import Alamofire

class SigninViewController: BaseViewController {

    @IBOutlet weak var editPassword: UITextField!
    @IBOutlet weak var editEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func onLogin() {
        
            if editEmail.text!.count == 0 {
                showError(message: "Email is mandatory")
            } else if editPassword.text!.count == 0 {
                showError(message: "Password is mandatory")
            } else {
                print(editEmail.text!)
                let body = [
                    "email": editEmail.text!,
                    "password": editPassword.text!
                ]
                
                let url = "http://172.18.6.111:4000/user/login"
                AF.request(url, method: .post, parameters: body,encoding: JSONEncoding())
                    .responseJSON(completionHandler: { response in
                            let result = response.value as! [String: Any]
                                        let status = result["status"] as! String
                        print("\(result)")
                        
                        if status == "success" {

                            let data = result["data"] as! [String: Any]
                            let id = data["userid"] as! Int
                            let name = data["username"] as! String

                            // persist the userId in user defaults
                            let userDefaults = UserDefaults.standard
                            userDefaults.setValue(id, forKey: "userid")
                            userDefaults.setValue(name, forKey: "username")
                            userDefaults.synchronize()

                            let transactionsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "RegistrationViewController")
                            self.navigationController?.pushViewController(transactionsVC, animated: true)

                        } else {
                            self.showError(message: "Invalid email or pasword")
                       }
                    })
            }
        }
    
    
}
